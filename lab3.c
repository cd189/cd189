#include<stdio.h>
int input()
{
	int x;
	scanf("%d",&x);
	return x;
}

int compute(int hours,int minutes)
{
	int time=0;
	time=minutes+(hours*60);
	return time;
}

void display(int time)
{
	printf("Time in minutes is %d",time);
}

int main()
{
	int a,b,result;
	printf("Enter hour:\n");
	a=input();
	printf("Enter minutes:\n");
	b=input();
	result=compute(a,b);
	display(result);
	return 0;
}
