//to print leap year
#include<stdio.h>
int input() 
{
	int x;
	scanf("%d",&x);
	return x;
}

int compute(int x)
{
	int flag;
	if(x%4==0 && x%100!=0)
	{
		flag=1;
	}
	else
 	{
		flag=0;
	}
	return flag;
}

void display(int flag)
{
	if(flag==1)
		{
			printf("GIVEN YEAR IS A LEAP YEAR");
		}
	else
	{
		printf("GIVEN YEAR IS NOT A LEAP YEAR");
	}
}
	
int main()
{
	int year,result;
	printf("Enter year:");
	year=input();
	result=compute(year);
	display(result);
	return 0;
}
}
	
//to print greatest number using ternary operator

#include<stdio.h>
int input()
{
	int x;
	scanf("%d",&x);
	return x;
}

int compute(int a,int b,int c)
{
	int A,B;
	A=a>b?a:b;
	B=A>c?A:c;
	printf("\n %d is greatest \n",B);
	return B;
}

int main()
{
	int d,e,f;
	printf("Enter three numbers:");
	d=input();
	e=input();
	f=input();
	compute(d,e,f);
	return 0;
}



