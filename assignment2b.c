#include<stdio.h>
#include<math.h>
int input()
{
	float a;
	printf("Enter value:");
	scanf("%f",&a);
	return a;
}
int compute(float x, float y,float z)
{
	float area;
	float s=((x+y+z)/2);
	area=sqrt(s*(s-x)*(s-y)*(s-z));
	return area;
}
void display(float area)
{
	printf("Area of given triangle %f",area);
}
int main()
{
	float c,d,e,z;
	c=input();
	d=input();
	e=input();
	z=compute(c,d,e);
	display(z);
	return 0;
}