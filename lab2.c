//to print the largest of three numbers 
#include<stdio.h>
int input()
{
	float x;
	printf("\nEnter value:\n");
	scanf("%f",&x);
	return x;
}
int compute(float a,float b,float c)
{
	int flag=0;
	if (a>=b && a>=c)
	{
		flag=1;
	}
	else if (b>=c)
	{
		flag=2;
	}
	else
	{
		flag=3;
	}
	return flag;
}

int main()
{
	float m,y,z;
	int flag=0;
	m=input();
	y=input();
	z=input();
	compute(m,y,z);
	if(flag==1)
	{
		printf("Largest number is %f\n",m);
	}
	else if(flag==2)
	{
		printf("Largest number is %f\n",y);
	}
	else
	{
		printf("Largest number is %f\n",z);
	}
	return 0;
}
