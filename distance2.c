#include<stdio.h>
#include<math.h>
int input()
{
	float x;
	scanf("%f",&x);
	return x;
}
float compute(float x1,float x2,float y1,float y2)
{
	float distance;
	distance=(sqrt(pow((x1-x2),2)+ pow((y1-y2),2)));
	return distance;
}

void display(float distance)
{
	printf("Distance between the given points in 2D plane is %f",distance);
}
	
int main()
{
	float x1,x2,y1,y2;
	float result;
	printf("Enter all the x1 and y1 co-ordinates \n");
	x1=input();
	y1=input();
	printf("Enter all the x2 and y2 co-ordinates \n");
	x2=input();
	y2=input();
	result=compute(x1,x2,y1,y2);
	display(result);
	return 0;
}
