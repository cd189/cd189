#include<stdio.h>
int input()
{
    int n;
    printf("\nEnter number:");
    scanf("%d",&n);
	return n;
}

int compute(int n)
{
	int m=1,i;
	for(i=1;i<=n;i++)
    {
        m=m*i;
    }
    return m;
}

void display(int m)
{
    
    printf("\nFactorial of given number is %d",m);
}


int main()
{
	int num=input();
	int result=compute(num);
	display(result);
	return 0;
}

