#include <stdio.h>
int main()
{
  int i, beg, end, mid, search;
  int array[6]={2,3,5,7,11,13};
  int n=6;

  for (i = 0; i< n; i++)
    {
    printf("%d\n",array[i]);
	}

  beg = 0;
  end = n - 1;
  mid = (beg+end)/2;
  search=11;

  while(beg <= end) {
    if (array[mid] < search)
      beg = mid + 1;
    else if (array[mid] == search) {
      printf("%d found at index %d\n", search, mid);
      break;
    }
    else
      end = mid - 1;

    mid = (beg + end)/2;
  }
  if (beg > end)
    printf("Not found in the array,%d isn't present \n", search);

  return 0;
}

