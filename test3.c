#include<stdio.h>
#define pi 3.14

int input()
{
	float a;
	printf("Enter radius:");
	scanf("%f",&a);
	return a;
}

float compute(float x)
{
	float area;
	area=(pi*x*x);
	return area;
}

void display(float area)
{
	printf("\n Area of given circle: %f\n",area);
}

int main()
{
	float r;
	float z;
	r=input();
	z=compute(r);
	display(z);
	return 0;
}
