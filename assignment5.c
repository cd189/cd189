#include<stdio.h>

int input()
{
	int x;
	scanf("%d",&x);
	return x;
}

int main()
{
     int i,a[50];
     printf("\nEnter the number of elements\n");
	 int n=input();
     printf("Enter the array\n");
     for(i=0;i<n;i++)
     {
         scanf("%d",&a[i]);
	 }    
    
    printf("\nEnter the position you want to delete\n");
    int pos;
    scanf("%d",&pos);
    if(pos>=n+1)
    {
        printf("\nDeletion is not possible");
        printf("\nGiven element position %d that the user entered is not in the array",pos);
    }
    
    else
    {
        for(i=pos-1;i<n-1;i++)
        {
            a[i]=a[i+1];
			printf("\nThe array after deletion of the element becomes: ");
			for(i=0;i<n-1;i++)
			{
				printf("\n%d\n",a[i]);
			}
		}
   return 0;
	}
}
