#include<stdio.h>
void add(int *a,int *b,int *s);
void sub(int *a,int *b,int *d);
void div(int *a,int *b,int *x);
void rem(int *a,int *b,int *y);

int main()
{
	int num1,num2,sum,diff,divn,r;
	printf("\nEnter two numbers:");
	scanf("%d %d",&num1,&num2);

	add(&num1,&num2,&sum);
	printf("\nSum of two numbers %d and %d = %d",num1,num2,sum);

	div(&num1,&num2,&divn);
	printf("\nDivision of two numbers %d and %d = %d",num1,num2,div);

	rem(&num1,&num2,&r);
	printf("\nRemainder of two numbers %d and %d = %d",num1,num2,r);

	sub(&num1,&num2,&diff);
	printf("\nDifference of two numbers %d and %d = %d",num1,num2,diff);

	return 0;
}

void add(int *a,int *b,int *s)
{
	*s=*a+*b;
}

void sub(int *a,int *b,int *d)
{
	*d=(*a)-(*b);
}

void div(int *a,int *b,int *x)
{
	*x=(*a)/(*b);
}

void rem(int *a,int *b,int *y)
{
	*y=(*a)%(*b);
}

