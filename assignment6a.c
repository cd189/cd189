#include<stdio.h>
#include<math.h>
int input()
{
	int n;
	printf("Enter value:");
	scanf("%d",&n);
	return n;
}

int compute(int y)
{
	int s;
	for(int i=2;i<=y;i+=2)
		{
			s=s+pow(i,2);
		}
		return s;
}

void display(float s)
{
	printf("\nSum of series of even numbers is  %f",s);
}

int main()
{
	int x,result;
	x=input();
	result=compute(x);
	display(result);
	return 0;
}
