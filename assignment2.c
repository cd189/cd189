#include<stdio.h>
int inputsalary()
{
	int salary;
	scanf("%d",&salary);
	return salary;
}

char gender()
{
	char a;
	scanf(" %c",&a);
	return a;
}

int compute(int salary,char a)
{
	float bonus;
	if(a=='m')
	{
		bonus=(0.05)*salary;
		return bonus;
	}
	else if(a=='f')
	{
		bonus=(0.1)*salary;
		return bonus;
	}
	else if((a=='m')&&(salary<=10000))
	{
		bonus=(0.05)+(0.02)*salary;
		return bonus;
	}
	else 
	{
		bonus=(0.1)+(0.02)*salary;
		return bonus;
	}
}

void display(float bonus)
{
	printf("Bonus of given worker: %f",bonus);
}

int main()
{
	float result;
	char gen;
	int sal;
	printf("\nEnter salary:");
	sal=inputsalary();
	printf("\nEnter gender(m/f):");
	gen=gender();
	result=compute(sal,gen);
	display(result);
	return 0;
} 
