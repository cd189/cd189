#include <stdio.h>
int summ(int);
int input()
{
	int x;
	scanf("%d",&x);
	return x;
}

int main()
{
  int n,r;
  printf("\nEnter number:\n");
  n=input();
  r = summ(n);
  printf("\nSum of digits of %d is %d\n",n,r);
  return 0;
}


int summ(int x) 
{
    int sum = 0;
	if (x == 0) 
	{
		return 0;
	}
	sum = x%10 + summ(x/10);
	{
		return sum;
	}
}

