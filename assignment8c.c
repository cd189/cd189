#include<stdio.h>
long compute(int d)
{
    long b=0;
    int rem, temp = 1;

    while (d!=0)
    {
        rem=d%2;
        d=d/2;
        b=b+rem*temp;
        temp=temp*10;
    }
    return b;
}
int main()
{
    int d;
    printf("Enter a number: ");
    scanf("%d", &d);
    printf("Binary Number of given decimal number is %ld",compute(d));
    return 0;
}
