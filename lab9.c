#include<stdio.h>
void swap(int *a,int *b)
{
	int t;
	t=*a;
	*a=*b;
	*b=t;
}

int main()
{
	int x,y;
	printf("\nEnter first number:");
	scanf("%d",&x);
		
	printf("\nEnter second number:");
	scanf("%d",&y);
		
	printf("\nBefore swapping x=%d and y=%d",x,y);
	
	swap(&x,&y);
		
	printf("\nAfter swapping x=%d and y=%d",x,y);
	return 0;
}
		
