#include<stdio.h>
int main()
{
	int row,col, matrix[10][10], transpose[10][10];
	printf("Enter number of rows and columns \n");
	scanf("%d%d", &row, &col);
	printf("Enter elements of the matrix \n");
	for(int i=0;i < row;i++)
		for(int j=0;j < col;j++)
			{
				printf("Enter element matrix%d%d=",i+1,j+1);
				scanf("%d", &matrix[i][j]);
			}

	printf("Entered matrix:\n");
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++) 
			{
				printf("%d\t", matrix[i][j]);
				if (j == col - 1)
				{	
					printf("\n");
				}
			}

	for(int i=0;i < row;i++)
		for(int j=0; j < col;j++)
			{
				transpose[j][i]=matrix[i][j];
			}
	printf("Transpose of the given matrix:\n");		
	for(int i=0;i < col;i++)
		for(int j=0 ;j < row;j++)
			{
				printf("%d\t", transpose[i][j]);
				if (j==row-1)
				{
					printf("\n");
				}
			}
	return 0;
}
